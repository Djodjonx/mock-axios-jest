# General

This directory was used by jest for mock node_modules

## Files

Each files mock a librairy. For mocking a lib, create file with the name of lib who's need to be imported.

for more details go to [jest documentation](https://jestjs.io/docs/en/manual-mocks)

Please describe your's mock after it was created.

## 1. Axios

This files was used for mocking Axios librairy.

Each methods like **get, post, put, patch and delete** were mocked.

Additionnals methods are provides for create reponse for each methods, create many reponses in the same time and reset response.

## functions:

__setMockResponse__:

Allow to create response for any CRUD methods.
Params needs to be like:
```
setMockResponse(method, url, data)
```

```
method => String
url => String
data => Object => default format {
  data: {},
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {},
}
```

for create an error, use status with statusCode errors. More details [http errors codes](https://httpstatuses.com/)


**example**
response:
```
setMockResponse('get', 'http://foo-bar.com', {data: { mySuperData: baz }})
```

error:
```
setMockResponse('put', 'http://foo-bar.com', {status: 404, data: {error: 'not found} })
```

__setManyMockResponse__:

Allow to create multiples response in same time.
params need to be an Array of array or object

```
method => String
url => String
data => Object => default format {
  data: {},
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {},
}
```


**example**

Array of array params:

```
setManyMockResponse([
  ['get', 'http://foo.com', {data: {bar: bar}}],
  ['post', 'http://bar.com', {status: 204}],
])
```
```
Array of Object params:
setManyMockResponse([
  {method: 'get', url:'http://foo.com', data:{data: {bar: bar}},
  {method: 'patch', urn: 'http://bar.com', data: {status: 500}},
])
```

__reset__:

Allow to reset all responses mocked.
Use this fonction after all test for remove all mock creates before.

### Warning

declared response for the same method and url `erase` the previous response.

### In Test

You can spy if axios is trigger and test params pass.

**example**
```
it('should send an ajax request with chosen options', () => {
  const myurl = 'http://www.fooBar.com'
  const myparams = 'foo'
  wrapper.vm.setOption()
  // test axios was call
  expect(axios.post).toHaveBeenCalled()
  // or / and
  // test axios was call with expected params
  expect(axios.get).toHaveBeenCalledWith(myurl, { myparams })
})
```
