import errorCodes from './helpers/errorCodes'

const responseModel = {
  data: {},
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {},
}
let mockResponses = {}

const axiosMock = jest.genMockFromModule('axios')

const hasError = (data) => {
  let statusCode = data.status
  if (typeof statusCode !== 'number') {
    statusCode = parseInt(statusCode, 10)
  }
  return errorCodes.indexOf(statusCode) > -1
}

const formatResponse = (data) => {
  const response = Object.assign({}, responseModel, data)
  if (hasError(response) && response.statusText === 'OK') {
    delete response.statusText
  }
  return response
}

const reponseSender = (method, url) => new Promise((resolve, reject) => {
  const response = formatResponse(mockResponses[method][url])
  if (hasError(response)) {
    reject(response)
  } else {
    resolve(response)
  }
})

const reqGet = async (url, params) => reponseSender('get', url, params)

const reqPost = async (url, params) => reponseSender('post', url, params)

const reqPut = async (url, params) => reponseSender('put', url, params)

const reqPatch = async (url, params) => reponseSender('patch', url, params)

const reqDelete = async (url, params) => reponseSender('delete', url, params)

axiosMock.get.mockImplementation(reqGet)

axiosMock.post.mockImplementation(reqPost)

axiosMock.put.mockImplementation(reqPut)

axiosMock.patch.mockImplementation(reqPatch)

axiosMock.delete.mockImplementation(reqDelete)

axiosMock.setMockResponse = (method, url, data) => {
  const methodIndex = method.toLowerCase()
  if (!mockResponses[methodIndex]) {
    mockResponses[methodIndex] = {}
  }
  mockResponses[methodIndex][url] = data
}

axiosMock.setManyMockResponse = (responseArray) => {
  responseArray.forEach((response) => {
    if (typeof response !== 'object') {
      axiosMock.setMockResponse(response.method, response.url, response.data)
    } else if (Array.isArray(response)) {
      axiosMock.setMockResponse(response[0], response[1], response[2])
    } else {
      throw new Error(`invalide format, response must be an array of object or
       array of arrays, for more details, read the README file ./src/__mocks__`)
    }
  })
}


axiosMock.finishRequest = () => { jest.runOnlyPendingTimers() }

axiosMock.reset = async () => {
  mockResponses = {}
  return true
}

export default axiosMock
